import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Profiles } from '../models/profiles.model';
import { Match } from '../models/match.model';
import { NewMatch } from '../models/newMatch.model';
import { Interest } from '../models/interest.model';
import { Profile } from '../models/profile.model';
import { NewProfile } from '../models/newProfile';

@Injectable()
export class ZinderService {
  path = 'http://localhost:8088/zinder';

  constructor(
    private http: HttpClient,
) {}

  getProfiles(): Observable<Profiles> {
    return this.http.get<Profiles>(`${this.path}/profils`);
  }

  matchProfile(idProfile: string, match: boolean): Observable<Match> {
    return this.http.post<Match>(`${this.path}/profils/${idProfile}/match`, new NewMatch(match));
  }

  getMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(`${this.path}/matchs`);
  }

  getInterests(): Observable<Interest[]> {
    return this.http.get<Interest[]>(`${this.path}/interets`);
  }

  deleteMatch(idMatch: string) {
    return this.http.delete(`${this.path}/matchs/${idMatch}`);
  }

  createProfil(newProfile: NewProfile): Observable<Profile> {
    return this.http.post<Profile>(`${this.path}/profils`, newProfile);
  }

}
