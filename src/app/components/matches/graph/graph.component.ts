import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Match } from 'src/app/models/match.model';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  @Input() matches: Match[];

  constructor() { }

  ngOnInit() {}


}
