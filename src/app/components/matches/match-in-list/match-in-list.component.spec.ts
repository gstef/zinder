import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchInListComponent } from './match-in-list.component';

describe('MatchInListComponent', () => {
  let component: MatchInListComponent;
  let fixture: ComponentFixture<MatchInListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchInListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchInListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
