import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Match } from 'src/app/models/match.model';
import { Profile } from 'src/app/models/profile.model';

@Component({
  selector: 'app-match-in-list',
  templateUrl: './match-in-list.component.html',
  styleUrls: ['./match-in-list.component.css']
})
export class MatchInListComponent implements OnInit, OnChanges {
  @Input() match: Match;
  @Input() profile: Profile;
  @Output() delete = new EventEmitter<string>();
  display = true;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (!this.match) {
      this.display = false;
    }
  }

  onClickDelete() {
    this.delete.emit(this.match.id);
  }

}
