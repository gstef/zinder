import { Component, OnInit, OnChanges } from '@angular/core';
import { Match } from 'src/app/models/match.model';
import { ZinderService } from 'src/app/service/zinder.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  matches: Match[];

  constructor(private zinterService: ZinderService) { }

  ngOnInit() {
    this.zinterService.getMatches().subscribe(response =>
      this.matches = response
      );
  }

  onClickDelete(idMatch: string){
    this.zinterService.deleteMatch(idMatch).subscribe( () =>
    this.zinterService.getMatches().subscribe(response =>
      this.matches = response
      )
    );
  }

}
