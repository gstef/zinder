import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Match } from 'src/app/models/match.model';
import { Profiles } from 'src/app/models/profiles.model';
import { ZinderService } from 'src/app/service/zinder.service';
import { Profile } from 'src/app/models/profile.model';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.css']
})
export class MatchListComponent implements OnInit, OnChanges {

  @Input() matches: Match[];
  @Output() delete = new EventEmitter<string>();
  profiles: Profiles;
  linkedProfiles: Profile[];

  constructor(private zinderService: ZinderService) { }

  ngOnInit() {
    this.zinderService.getProfiles().subscribe(response => {
      this.profiles = response;
      this.linkedProfiles = this.matches.map(match => this.linkedProfileTo(match.profil));
    });
  }

  ngOnChanges(){
    this.zinderService.getProfiles().subscribe(response => {
      this.profiles = response;
      this.linkedProfiles = this.matches.map(match => this.linkedProfileTo(match.profil));
    });
  }


  linkedProfileTo(id: string){
    return this.profiles.profils.find(profile => profile.id === id);
  }

  onClickDelete(s){
    this.delete.emit(s);
  }

}
