import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile.model';
import { ZinderService } from 'src/app/service/zinder.service';
import { Interest } from 'src/app/models/interest.model';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  profiles: Profile[];
  interests: Interest[];
  filteredProfiles: Profile[];
  selectedInterestName: string;

  constructor(private zinderService: ZinderService) { }

  ngOnInit() {
    this.zinderService.getInterests().subscribe(response =>
      this.interests = response
    );
    this.zinderService.getProfiles().subscribe( response => {
      this.profiles = response.profils;
      this.filteredProfiles = this.profiles;
    }
    );
  }

  findInterestsof(profile: Profile) {
    const result: Interest[] = [];
    profile.interets.forEach(id => result.push(this.interests.find(interest => interest.id === id)));
    return result;
  }

  onChangeFilter() {
    const selectedInterest: Interest =
    this.interests.find(interest => interest.nom === this.selectedInterestName);

    this.filteredProfiles = this.profiles.filter(profile =>
      profile.interets.includes(selectedInterest.id)
      );
  }

}
