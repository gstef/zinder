import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'src/app/models/profile.model';
import { ZinderService } from 'src/app/service/zinder.service';
import { Interest } from 'src/app/models/interest.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() profile: Profile;
  @Input() interests: Interest[];

  constructor(private zinderService: ZinderService) { }

  ngOnInit() {
  }

  sendMatch(b: boolean) {
    this.zinderService.matchProfile(this.profile.id, b).subscribe();
  }



}
