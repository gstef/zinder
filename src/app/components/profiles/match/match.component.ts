import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  @Output() buttonClicked = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  onClickEmit(b: boolean){
    this.buttonClicked.emit(b);
  }

}
