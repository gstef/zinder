import { Component, OnInit } from '@angular/core';
import { Interest } from 'src/app/models/interest.model';
import { ZinderService } from 'src/app/service/zinder.service';
import { NewProfile } from 'src/app/models/newProfile';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {
  interests: Interest[];

  nom: string;
  prenom: string;
  photoUrl: string;
  checkedInterests: Interest[];

  constructor(private zinderService: ZinderService) { }

  ngOnInit() {
    this.zinderService.getInterests().subscribe(response => this.interests = response);
  }

  onClickSend() {
    this.zinderService.createProfil(new NewProfile(this.nom, this.prenom, this.photoUrl, []));
  }

}
