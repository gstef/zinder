import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProfileComponent } from './components/profiles/profile/profile.component';
import { ZinderService } from './service/zinder.service';
import { ProfilesComponent } from './components/profiles/profiles/profiles.component';
import { HttpClientModule } from '@angular/common/http';
import { MatchComponent } from './components/profiles/match/match.component';
import { MatchesComponent } from './components/matches/matches/matches.component';
import { GraphComponent } from './components/matches/graph/graph.component';
import { MatchListComponent } from './components/matches/match-list/match-list.component';
import { MatchInListComponent } from './components/matches/match-in-list/match-in-list.component';
import { AdministrationComponent } from './components/administration/administration.component';

const appRoutes: Routes = [
  { path: 'profiles', component: ProfilesComponent },
  { path: 'stats', component: MatchesComponent },
  { path: 'admin', component: AdministrationComponent },
  { path: '',
    redirectTo: '/profiles',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    ProfileComponent,
    ProfilesComponent,
    MatchComponent,
    MatchesComponent,
    GraphComponent,
    MatchListComponent,
    MatchInListComponent,
    AdministrationComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [ZinderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
